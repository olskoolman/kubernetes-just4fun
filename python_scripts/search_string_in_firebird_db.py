import fdb
import argparse
from pathlib import Path
import os
from configparser import ConfigParser

def connect_to_db():
    conn = None
    config_file_section = "firebird"
    config_file_root = Path(__file__).resolve().parents[0]
    config_file_path = Path(config_file_root, "db_connection.ini")
    db_info = {}
    if os.path.exists(config_file_path):
        parser = ConfigParser()
        parser.read(config_file_path)
        if parser.has_section(config_file_section):
            key_values = parser.items(config_file_section)
            for item in key_values:
                db_info[item[0]] = item[1]
    else:
        db_info["host"] = os.environ['DB_HOST'],
        db_info["database"] = os.environ['DB_DATABASE'],
        db_info["user"] = os.environ['DB_USER'],
        db_info["password"] = os.environ['DB_PASSWORD']
        db_info["port"] = os.environ['DB_PORT']
    try:
        conn = fdb.connect(**db_info)
    except (Exception, fdb.DatabaseError) as error:
        print(error)
    return conn

def search_string_in_database(cur, search_string, skip_tables, skip_columns):
    found_rows = []
    
    print("==> Get tables list")
    cur.execute("SELECT TRIM(RDB$RELATION_NAME) FROM RDB$RELATIONS WHERE RDB$SYSTEM_FLAG = 0")
    tables = cur.fetchall()

    for table in tables:
        table_name = table[0]
        table_name = table_name.strip()
        print(f"===> Get columns of {table_name}")
        
        if table_name in skip_tables:
            continue
        
        cur.execute("SELECT TRIM(RDB$FIELD_NAME) FROM RDB$RELATION_FIELDS WHERE RDB$RELATION_NAME = ?", (table_name,))
        columns = cur.fetchall()
        
        for column in columns:
            column_name = column[0]
            column_name = column_name.strip()
            if column_name in skip_columns:
                continue
            print(f"===> Searching in columns... {table_name} - {column_name}")
            query = 'SELECT "' + column_name + '" FROM "' + table_name + '" WHERE "' + column_name + '" CONTAINING ?'
            try:
                cur.execute(query, (search_string,))
                rows = cur.fetchall()
                for row in rows:
                    found_rows.append((table_name, column_name, row[0]))
            except fdb.fbcore.DatabaseError as e:
                print(f"Error searching {table_name}, column {column_name}: {e}")
                continue
    return found_rows

def parse_arg_values():
    search_string = ""
    skip_columns = []
    skip_tables = []
    parser = argparse.ArgumentParser(description='Draw graf for memory usage from top')
    parser.add_argument('--searchstring', type=str, help='STRING looking for in the database')
    parser.add_argument('--skipcolums', type=str, help='Comma-separated list of columns should not be process')
    parser.add_argument('--skiptables', type=str, help='Comma-separated list of tables should not be process')
    args = parser.parse_args()
    if args.path:
        search_string = args.searchstring
    if args.services:
        skip_columns = parse_arg_lists(args.skipcolums)
    if args.max_memory:
        skip_tables = parse_arg_lists(args.skiptables)
    return search_string, skip_columns, skip_tables

def parse_arg_lists(args):
    args = args.split(',')
    return args

if __name__ == "__main__":
    find_string, skip_columns, skip_tables = parse_arg_values()
    conn = connect_to_db()
    cur = conn.cursor()
    results = search_string_in_database(cur, find_string)
    if results:
        print(f"===> Found '{find_string}':")
        for table_name, column_name, value in results:
            print(f"Table: {table_name}, Column: {column_name}, Value: {value}")
    else:
        print("===>THERE IS NO RESULTS")
    cur.close()
    conn.close()
