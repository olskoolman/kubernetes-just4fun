from datetime import datetime
from pathlib import Path
from plotly.subplots import make_subplots
import os
import plotly.graph_objects as go
import re
import argparse
import pandas as pd

def read_file(file_path):
    content = open(file_path)
    return content

def get_files(root_path):
    file_paths = []
    for file in os.listdir(root_path):
        file_path = Path(root_path, file)
        file_paths.append(file_path)
    return file_paths

def parse_files(file_paths, services, max_memories):
    if len(services) != len(max_memories):
        max_memory = max_memories[0]
        for item in range(len(services)):
            max_memories[item] = max_memory
    df_columns = ["Date", "System_memory", "System_swap"]
    df_columns = df_columns + services
    values = pd.DataFrame(columns=df_columns)
    for file_path in file_paths:
        row_data = {}
        file_content = read_file(file_path)
        for line in file_content:
            if re.match('^\[.*', line):
                if len(row_data) != 0:
                    values = values._append(row_data, ignore_index=True)
                    row_data = {}
                time = get_time(line)
                row_data['Date'] = time
            elif re.match('^KiB.Mem.*', line):
                memory_usage = get_usage(line, 'Mem :')
                row_data['System_memory'] = memory_usage
            elif re.match('^KiB.Swap.*', line):
                swap_usage = get_usage(line, 'Swap:')
                row_data['System_swap'] = swap_usage
            else:
                for service_num in range(len(services)):
                    service_name = services[service_num]
                    max_memory = float(max_memories[service_num])
                    if re.match(rf'.*{service_name}', line):
                        memory_usage = get_process_usage(line, max_memory)
                        row_data[service_name] = memory_usage
    return values

def get_usage(line, total_begin):
    line_values = line.split(",")
    total_begin = str(re.escape(total_begin))
    total_end = str(re.escape('total'))
    used_end = str(re.escape('used'))
    total = float(re.findall(total_begin+"(.*)"+total_end, line_values[0])[0])
    used = float(re.findall("(.*)"+used_end, line_values[2])[0])
    percentage = round((used / total) * 100, 2)
    return(percentage)

def get_process_usage(line, max_memory):
    line = re.sub(r'\s+', ' ', line)
    line_values = line.split(' ')
    reserved_memory = line_values[5]
    if line_values[0] == '':
        reserved_memory = line_values[6]
    used_memory = 0
    if re.match('.*[gG]$', reserved_memory):
        used_memory = float(reserved_memory[:-1])
    elif re.match('.*[mM]$', reserved_memory):
        used_memory = float(reserved_memory[:-1]) / 1024
    elif len(reserved_memory) > 3:
        used_memory = float(reserved_memory) / 1024 / 1024
    else:
        used_memory = float(reserved_memory)
    percentage = round((used_memory / max_memory) * 100, 2)
    if percentage > 100:
        print(f'{line}\nused memory: {used_memory}, max memory: {max_memory}')
    return(percentage)

def draw(values):
    fig = make_subplots(
        rows=2, cols=1
    )
    for column_num in range(1, len(values.columns)):
        row = 2
        if column_num < 3:
            row = 1 
        fig.add_trace(
            go.Scatter(
                x=values['Date'].to_list(),
                y=values.iloc[:,column_num].to_list(),
                name=values.columns[column_num]
            ),
            row=row, col=1
        )
    fig.show()

def get_time(line):
    time = ''.join(line.split('[')[1].split(']')[0])
    time = datetime.strptime(time, '%Y-%m-%d %H:%M:%S')
    return time

def parse_arg_values():
    root_path = ""
    services = []
    max_memories = []
    parser = argparse.ArgumentParser(description='Draw graf for memory usage from top')
    parser.add_argument('--path', type=str, help='Path to log files contains top output')
    parser.add_argument('--services', type=str, help='Comma-separated list of services should process')
    parser.add_argument('--max_memory', type=str, help='Comma-separated list of max memory values for services (same order)')
    args = parser.parse_args()
    
    if args.path:
        root_path = Path(args.path)
    if args.services:
        services = parse_arg_lists(args.services)
    if args.max_memory:
        max_memories = parse_arg_lists(args.max_memory)
    return root_path, services, max_memories

def parse_arg_lists(args):
    args = args.split(',')
    return args

if __name__ == '__main__':
    root_path, services, max_memories = parse_arg_values()
    values_for_draw = parse_files(get_files(root_path), services, max_memories)
    draw(values_for_draw)