
Role Name
=========

Ubuntu-host
-----------

This role prepared for create Ubuntu based server in different typpe of virtualized environments (Hyper-V, KVM... etc.)

Requirements
------------

* Ansible
* Jinja2

Role Variables
--------------

### Values from inventory for all hosts

* hypervisor_node_name: The hypervisor node FQDN/IP, where the virualization environment is manageable
* ip4_address: IP address of VM
* ip4_default_gateway: gateway IP address for VM's IP settings
* ip4_dns: dns server IP addresses for VM's IP settings
* ip4_subnet_length: subnet mask lenght equal to **{{ ip4_subnet_mask }}**
* search_domain: the domain name which the VM is contained
* temp_dir_name: directory name without path
* ubuntu_version: The version of Ubuntu is used for installation process (VM)
* vm_user_name: default user created during VM installation process

### Values from inventory for specified host

* host_ip:
* vm_host_name:

### Values from Vault

* vm_ssh_key: ssh key applied for authentication on installed VM ()
* vm_user_password: encrypted password for default VM user

### defaults/main.yml

* keyboard_layout: value should be accepted by Ubunutu installer (used in **templates/user-data.j2**)
* os_locale: value should be accepted by Ubunutu installer (used in **templates/user-data.j2**)
* keyboard_variant: value should be accepted by Ubunutu installer (used in **templates/user-data.j2**)

### vars/main.yml

* ubunutu_iso_url: The url for Ubuntu iso to download
* working_temp_directory: Directory for custom iso creation (default user home directory + **{{ temp_dir_name }}**)
* host_fqdn: default is **{{ vm_host_name }}.{{ search_domain }}**

Dependencies
------------

### For hyper-v management
```
ansible-galaxy collection install ansible.windows
```

### For iso creation
```
ansible-galaxy collection install community.general
```

Example Playbook
----------------

```
- hosts: all
  become: false
  gather_facts: false
  serial: 3

  vars:
    debug_tasks: true
    hypervisor_name: "hyperv"

  roles:
    - role: ubuntu-host
      vars:
        ubuntu_version: 22.04.3
        vm_user_name: "" # add users during install process follow changes in templates # README
        vm_user_name2: "" # depend on how many users we would like to add durng install process
        vm_user_name3: ""
        os_locale: "en_US"
        keyboard_layout: "hu"
        keyboard_variant: "standard"
        ip4_subnet_length: 20
        ip4_default_gateway: 10.10.10.10 # ip of the gateway
        ip4_dns: 10.10.10.10 # ip of dns server
        SearchDomain: "localhost.hu" 
```

License
-------
MIT

Author Information
------------------
illes.tamas@itdev.hu
