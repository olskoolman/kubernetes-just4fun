Role Name
=========

## docker-vm

This role prepared for install required packages for Docker environmenmt.

Requirements
------------

* Ansible

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

```
- hosts: all
  become: true
  gather_facts: true

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: docker-vm
```

License
-------

MIT

Author Information
------------------

illes.tamas@itdev.hu
