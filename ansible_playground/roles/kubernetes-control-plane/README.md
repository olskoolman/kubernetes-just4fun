Role Name
=========

## kubernetes-control-plane

This role prepared for handle control planes for kubernetes cluster

Requirements
------------

* Ansible
* Jinja2

Role Variables
--------------

### Values from inventory for all hosts

* kube_context: name of kubernetes context
* kube_subversion: sub version of kubernetes (depends on distro repository's numbering concept)
* kube_version: main version of kubernetes (depends on distro repository's numbering concept) (**1.27.3-00**)
* kubernetes_cluster_entry_name: kubernetes API name (**{{ kubernetes_cluster_entry_name }}.{{}}:{{ kubernetes_api_port }}**)
* pod_subnet: subnet for pods within kubernetes cluster (**"10.85.0.0/16"**)
* search_domain: domain name used by nodes

### defaults/main.yml

* kubernetes_api_port: port for access API (default: 6443)

### vars/main.yml

* join_command: "" (it is required to be intialized with empty value)
* cert_key_for_join: "" (it is required to be intialized with empty value)

Dependencies
------------

Example Playbook
----------------

```
- hosts: kubernetes_control_planes,kubernetes_worker_nodes
  become: true
  gather_facts: true

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: kubernetes-node

- hosts: kubernetes_control_planes
  become: true
  gather_facts: true
  serial: 1

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: kubernetes-control-plane

- hosts: kubernetes_worker_nodes
  become: true
  gather_facts: true
  serial: 1

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: kubernetes-worker-node
```

License
-------

MIT

Author Information
------------------

illes.tamas@itdev.hu
