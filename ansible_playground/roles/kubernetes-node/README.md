Role Name
=========

## kubernetes-control-plane

This role for prepare nodes for running kubernetes cluster

Requirements
------------

* Ansible
* Jinja2

Role Variables
--------------

### Values from inventory for all hosts

* kube_node_name: the name of node without domain
* os_name: the os name the node installed with ("suse", "ubuntu", "debian")
* kube_context: name of kubernetes context
* kube_version: main version of kubernetes (depends on distro repository's numbering concept) (**1.27.3-00**)
* kube_subversion: sub version of kubernetes (depends on distro repository's numbering concept)
* search_domain: domain name used by nodes
* kubernetes_cluster_entry_name: kubernetes API name (**{{ kubernetes_cluster_entry_name }}.{{}}:{{ kubernetes_api_port }}**)
* ansible_host: the name of node without domain (yes, duplicated)
* vm_host_name: the name of node without domain (yes, triplecated)

### vars/main.yml

* host_ip: **"{{ ansible_host }}"**
* host_fqdn: **"{{ vm_host_name }}.{{ search_domain }}"**

Dependencies
------------

Example Playbook
----------------

```
- hosts: kubernetes_control_planes,kubernetes_worker_nodes
  become: true
  gather_facts: true

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: kubernetes-node

- hosts: kubernetes_control_planes
  become: true
  gather_facts: true
  serial: 1

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: kubernetes-control-plane

- hosts: kubernetes_worker_nodes
  become: true
  gather_facts: true
  serial: 1

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: kubernetes-worker-node
```

License
-------

MIT

Author Information
------------------

illes.tamas@itdev.hu
