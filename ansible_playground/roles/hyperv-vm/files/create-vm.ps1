function CreateHyperVVM {
    param (
        $vm_host_name,
        $vm_memory_size,
        $vm_storage_size,
        $hypervisor_virual_switch_name,
        $hypervisor_node_name,
        $vm_store_path
    )
    $hyperv_parameters = Get-VMHost
    $vhdx_name = $vm_host_name + ".VHDX"

    if ( !$vm_store_path ) {
        $vhd_path = Join-Path -Path $hyperv_parameters.VirtualHardDiskPath $vhdx_name
        $vm_path = Join-Path -Path $hyperv_parameters.VirtualMachinePath $vm_host_name
    }
    else {
        $vhd_path = Join-Path -Path $vm_store_path $vm_host_name
        $vhd_path = Join-Path -Path $vhd_path "Virtual Hard Disk"
        $vhd_path = Join-Path -Path $vhd_path $vhdx_name
        $vm_path = Join-Path -Path $vm_store_path $vm_host_name
    }

    $vhd_exist = Get-VHD -Path $vhd_path
    if (-Not $vhd_exist) {
        New-VHD -Path $vhd_path -Dynamic -SizeBytes $vm_storage_size
    }

    $vm_parameters = @{
        Name = $vm_host_name
        MemoryStartupBytes = $vm_memory_size
        Generation = 2
        VHDPath = $vhd_path
        Path = $vm_path
        SwitchName = $hypervisor_virual_switch_name
    }

    $vm_exist = Get-VM -Name $vm_host_name -ErrorAction SilentlyContinue
    if (-Not $vm_exist) {
        New-VM @vm_parameters
        $DVD = Add-VMDvdDrive -VMName $vm_host_name -Path $ISOPath -Passthru
        Set-VMFirmware -VM $vm_host_name -FirstBootDevice $DVD
        Set-VM -Name $vm_host_name -AutomaticCheckpointsEnabled $false
    }
}

CreateHyperVVM -vm_host_name $args[0] -vm_memory_size $args[1] -vm_storage_size $args[2] -hypervisor_virual_switch_name $args[3] -hypervisor_node_name $args[4] -vm_store_path $args[5]