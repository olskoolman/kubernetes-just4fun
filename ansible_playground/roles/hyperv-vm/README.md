Role Name
=========

Hyperv-vm
-----------

This role prepared for create virtual machine on Hyper-V virtualization platform. It is contains tasks related to handling virtual machines on Hyper-V

Requirements
------------

* Ansible
* Jinja2

Role Variables
--------------

### Values from inventory for all hosts

* vm_user_name: default user created during VM installation process, and used by Ansible for connection
* hypervisor_virual_switch_name: the network switch on Hyper-V used by virtual machine

### Values from inventory for specified host

* vm_core_num: vcore number for virtual machine
* vm_host_name: name for virtual machine
* vm_memory_size: virtual machine memory size
* vm_storage_size: the size of virtual storage for virtual machine
* vm_store_path: path where vhdx for virtual machine is stored

### defaults/main.yml

* temp_dir_name: directory name without path

### vars/main.yml

* working_temp_directory: Directory for custom iso creation (default user home directory + **{{ temp_dir_name }}**)
* hypervisor_node_name: The hypervisor node FQDN/IP, where the virualization environment is manageable

Dependencies
------------

### For hyper-v management
```
ansible-galaxy collection install ansible.windows
```

Example Playbook
----------------

```
- hosts: all
  become: false
  gather_facts: false

  roles:
  - role: hyperv-vm
    vars:
      hyperv_node_name: 10.10.10.10 # ip for hyperv node
      hyperv_cluster_name: ""
      hypervisor_virual_switch_name: WSL # hyperv switch name
      ansible_user: "" # user name for hyperv node
      ansible_password: "{{ hypervisor_user_password }}"
      ansible_connection: winrm # README !!!
      ansible_winrm_transport: ntlm
      ansible_winrm_server_cert_validation: ignore
      vm_store_path: "" # "d:\\VirtualMachines"
```

License
-------

MIT

Author Information
------------------

illes.tamas@itdev.hu
