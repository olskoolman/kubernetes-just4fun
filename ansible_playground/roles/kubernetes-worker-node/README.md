Role Name
=========

## kubernetes-worker-node

This role prepared for initialize worker nodes for kubernetes cluster

Requirements
------------

* Ansible
* Jinja2

Role Variables
--------------

### vars/main.yml

* join_command: "" (it is required to be intialized with empty value)
* cert_key_for_join: "" (it is required to be intialized with empty value)

Dependencies
------------

Example Playbook
----------------

```
- hosts: kubernetes_control_planes,kubernetes_worker_nodes
  become: true
  gather_facts: true

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: kubernetes-node

- hosts: kubernetes_control_planes
  become: true
  gather_facts: true
  serial: 1

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: kubernetes-control-plane

- hosts: kubernetes_worker_nodes
  become: true
  gather_facts: true
  serial: 1

  vars_files:
    - "vault/vm_secrets_template.yml"

  roles:
    - role: kubernetes-worker-node
```

License
-------

MIT

Author Information
------------------

illes.tamas@itdev.hu
