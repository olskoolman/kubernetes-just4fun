Role Name
=========

Debian-host
-----------

This role prepared for create Debian based server in different typpe of virtualized environments (Hyper-V, KVM... etc.)

Requirements
------------

* Ansible
* Jinja2

Role Variables
--------------

### Values from inventory for all hosts

* ip4_default_gateway: gateway IP address for VM's IP settings
* ip4_dns: dns server IP addresses for VM's IP settings
* ip4_subnet_length: subnet mask lenght equal to **{{ ip4_subnet_mask }}**
* ip4_subnet_mask: subnet mask of the VM (eg. 255.255.255.192)
* search_domain: the domain name which the VM is contained
* temp_dir_name: directory name without path
* vm_user_name: default user created during VM installation process
* debian_version: The version of Debian is used for installation process (VM)

### Values from inventory for specified host

* host_ip: 
* vm_host_name:

### Values from Vault

* root_password: encrypted password for root VM user
* vm_ssh_key: ssh key applied for authentication on installed VM ()
* vm_user_password: encrypted password for default VM user

### defaults/main.yml

* keyboard_layout: value should be accepted by Debian installer (used in **templates/pressed.cfg.j2**)
* os_locale: value should be accepted by Debian installer (used in **templates/pressed.cfg.j2**)
* time_zone: value should be accepted by Debian installer (used in **templates/pressed.cfg.j2**)

### vars/main.yml

* debian_iso_url: The url for Debian iso to download
* hypervisor_node_name: The hypervisor node FQDN/IP, where the virualization environment is manageable
* working_temp_directory: Directory for custom iso creation (default user home directory + **{{ temp_dir_name }}**)
* host_fqdn: default is **{{ vm_host_name }}.{{ search_domain }}**

Dependencies
------------

### For hyper-v management
```
ansible-galaxy collection install ansible.windows
```

### For iso creation
```
ansible-galaxy collection install community.general
```

Example Playbook
----------------

```
- hosts: all
  become: false
  gather_facts: false
  serial: 3 # depend on environment performance :)

  vars_files:
  - "vault/vm_secrets_template.yml"

  vars:
    debug_tasks: true

  roles:
  - role: debian-host
    vars:
      debian_version: 12.2.0
      vm_user_name: "" # add users during install process follow changes in templates # README
      vm_user_name2: "" # depend on how many users we would like to add durng install process
      vm_user_name3: ""
      time_zone: "Europe/Budapest"
      os_locale: "en_US"
      keyboard_layout: "hu"
      ip4_subnet_length: 20 # length of subnet
      ip4_subnet_mask: 255.255.255.255
      ip4_default_gateway: 10.10.10.10 # ip of the gateway
      ip4_dns: 10.10.10.10 # ip of dns server
      search_domain: "localhost.com" # domain name extension
```

License
-------

MIT

Author Information
------------------

illes.tamas@itdev.hu
