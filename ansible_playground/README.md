# OS installation roles

## Required packages

* whois
* sshpass
* p7zip-full
* pycdlib
* xorriso
* isolinux

## Create encrypted password for role variables

```mkpasswd -m sha-512 -s```

## Accept the ssh key of new VMs
On localhost:
```
export ANSIBLE_HOST_KEY_CHECKING=False
```

Or in ansible.cfg
```
[defaults]
host_key_checking=False
```

# Hypervisor management roles

## Required packages for Windows management (Hyper-V)

* pywinrm

## winrm setup on hyper-v host

[winrm for Ansible](https://docs.ansible.com/ansible/latest/os_guide/windows_setup.html)

### If there is no valid certificate on host
Run the following powershell command for manage hyper-v without encryption:
```Set-Item -Path WSMan:\localhost\Service\AllowUnencrypted -Value $true```

Add the following variables for Ansible:
```
ansible_port=5985
ansible_winrm_scheme='http'
```

### In case the valid certificate is exist on host but there is no listener for encrypted management
Run the following powershell command on hyper-v host:
```winrm quickconfig -transport:https```


# Create multiple users during the install process

``` roles/ubuntu-vm/templates/user-data.j2 ```

A következő szekcióba kell beilleszteni a szükséges usereket (változókat és vault-ot is hozzá kell igazítani):

## UBUNTU
```
  user-data:
    hostname: {{ vm_host_name }}
    users:
    - name: {{ vm_user_name }}
      passwd: {{ vm_user_password }}
      shell: /bin/bash
      lock-passwd: false
      groups: [adm, cdrom, dip, plugdev, lxd, sudo]
    - name: {{ vm_user_name2 }}
      passwd: {{ vm_user_password2 }}
      shell: /bin/bash
      lock-passwd: false
      groups: [adm, cdrom, dip, plugdev, lxd, sudo]
```

## SUSE
```
  <user_defaults t="map">
    <expire/>
    <group>100</group>
    <home>/home</home>
    <inactive>-1</inactive>
    <shell>/bin/bash</shell>
    <umask>022</umask>
  </user_defaults>
  <users t="list">
    <user t="map">
      <encrypted t="boolean">true</encrypted>
      <fullname>{{ vm_user_name }}</fullname>
      <home>/home/{{ vm_user_name }}</home>
      <home_btrfs_subvolume t="boolean">false</home_btrfs_subvolume>
      <shell>/bin/bash</shell>
      <user_password>{{ vm_user_password }}</user_password>
      <authorized_keys config:type="list">
        <listentry>{{ vm_ssh_key }}</listentry>
      </authorized_keys>
      <username>{{ vm_user_name }}</username>
    </user>
    <user t="map">
      <authorized_keys t="list"/>
      <encrypted t="boolean">true</encrypted>
      <fullname>root</fullname>
      <gid>0</gid>
      <home>/root</home>
      <home_btrfs_subvolume t="boolean">false</home_btrfs_subvolume>
      <password_settings t="map">
        <expire/>
        <flag/>
        <inact/>
        <max/>
        <min/>
        <warn/>
      </password_settings>
      <shell>/bin/bash</shell>
      <uid>0</uid>
      <user_password>{{ root_password }}</user_password>
      <username>root</username>
    </user>
  </users>
```