# **issue:** cannot manage network connection from gui, always ask for root password (network-manager)
*solution:*
`vim /usr/share/polkit-1/actions/org.freedesktop.NetworkManager.policy`
edit lines like:
from:
```
    <defaults>
      <allow_any>auth_admin_keep</allow_any>
      <allow_inactive>auth_admin_keep</allow_inactive>
      <allow_active>auth_admin_keep</allow_active>
    </defaults>
```
to
```
    <defaults>
      <allow_any>yes</allow_any>
      <allow_inactive>yes</allow_inactive>
      <allow_active>yes</allow_active>
    </defaults>
```

`systemctl restart networking`
