# VNC Server setup

# Install packages

tigervnc-server (tigervnc-standalone-server)

# Configuration

## Global configs

- `vim /etc/tigervnc/vncserver-config-defaults` to set accept only local connections (connect via ssh tunnel)

```
## Default settings for VNC servers started by the vncserver service
#
# Any settings given here will override the builtin defaults, but can
# also be overriden by ~/.vnc/config and vncserver-config-mandatory.
#
# See HOWTO.md and the following manpages for more details:
#     vncsession(8) Xvnc(1)
#
# Several common settings are shown below. Uncomment and modify to your
# liking.

session=plasmax11
# securitytypes=vncauth,tlsvnc
# geometry=2000x1200
# localhost
# alwaysshared
localhost=yes # <== ONLY ACCEPT LOCAL CONNECTIONS!
```

or (see the config)

```
$localhost = "yes";
```

- `vim /etc/tigervnc/vncserver-config-mandatory` just for note, no settings neccessarry
- `vim /etc/tigervnc/vncserver.users` set the server number for users

```
# TigerVNC User assignment
#
# This file assigns users to specific VNC display numbers.
# The syntax is <display>=<username>. E.g.:
#
# :2=andrew
# :3=lisa
:1=USERNAME
```

## User config

- `vim /home/USERNAME/.vnc/config` for no authentication and local connection only (authentication handeld by ssh connection)

```
## Supported server options to pass to vncserver upon invocation can be listed
## in this file. See the following manpages for more: vncserver(1) Xvnc(1).
## Several common ones are shown below. Uncomment and modify to your liking.
##
# securitytypes=vncauth,tlsvnc
# desktop=sandbox
# geometry=2000x1200
# localhost
alwaysshared
localhost
SecurityTypes=None
```

- `vim /home/USERNAME/.vnc/xstartup` # set the starting environment, for example settings for lxqt
- `chmod +x /home/USERNAME/.vnc/xstartup`

```
#!/bin/sh

# Start up the standard system desktop
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS

# /usr/bin/startxface4
# /usr/bin/cinnamon-session
# /usr/bin/gnome-session
/usr/bin/startlxqt
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
x-window-manager &
```

or for kde-plasma

```
#!/bin/sh
# Run a generic session
if [ -z "$MODE" ]
then
    xsetroot -solid grey &   #set the background picture
    export XKB_DEFAULT_RULES=base & #both should be needed for keyboard signals
    export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb & 


    export $(dbus-launch) &
    exec startplasma-x11&
fi
```

## Set vnc server run as service

- `vim /usr/lib/systemd/system/vncserver@.service`

**Fedora**
```
# The vncserver service unit file
#
# Quick HowTo:
# 1. Add a user mapping to /etc/tigervnc/vncserver.users.
# 2. Adjust the global or user configuration. See the
#    vncsession(8) manpage for details. (OPTIONAL)
# 3. Run `systemctl enable vncserver@:<display>.service`
# 4. Run `systemctl start vncserver@:<display>.service`
#
# DO NOT RUN THIS SERVICE if your local area network is
# untrusted!  For a secure way of using VNC, you should
# limit connections to the local host and then tunnel from
# the machine you want to view VNC on (host A) to the machine
# whose VNC output you want to view (host B)
#
# [user@hostA ~]$ ssh -v -C -L 590N:localhost:590M hostB
#
# this will open a connection on port 590N of your hostA to hostB's port 590M
# (in fact, it ssh-connects to hostB and then connects to localhost (on hostB).
# See the ssh man page for details on port forwarding)
#
# You can then point a VNC client on hostA at vncdisplay N of localhost and with
# the help of ssh, you end up seeing what hostB makes available on port 590M
#
# Use "nolisten=tcp" to prevent X connections to your VNC server via TCP.
#
# Use "localhost" to prevent remote VNC clients connecting except when
# doing so through a secure tunnel.  See the "-via" option in the
# `man vncviewer' manual page.


[Unit]
Description=Remote desktop service (VNC)
After=syslog.target network.target systemd-user-sessions.service

[Service]
Type=forking
ExecStartPre=+/usr/libexec/vncsession-restore %i
ExecStart=/usr/libexec/vncsession-start %i
PIDFile=/run/vncsession-%i.pid
SELinuxContext=system_u:system_r:vnc_session_t:s0

[Install]
WantedBy=multi-user.target
```

**Debian**

```
[Unit]
Description=Remote desktop service (VNC)
After=network.target systemd-user-sessions.service

[Service]
Type=forking
ExecStart=/usr/libexec/tigervncsession-start %i
PIDFile=/run/tigervncsession-%i.pid
SELinuxContext=system_u:system_r:vnc_session_t:s0

[Install]
WantedBy=multi-user.target
```

- `systemctl daemon-reload`
- `systemctl enable vncserver@\:1.service`
- `systemctl start vncserver@\:1.service`