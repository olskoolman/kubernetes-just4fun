function GetKubeNamespaceNames
{
    $namespace_names = New-Object System.Collections.ArrayList
    $kubectl_namespaces = kubectl get namespaces --no-headers -o custom-columns=":metadata.name"
    foreach ($namespace_name in $kubectl_namespaces)
    {
        if($namespace_name -ne "default" -And $namespace_name -ne "kube-system")
        {
            #void neccessary because the array contains indeces without it 
            [void]$namespace_names.Add($namespace_name)
        }
    }
    return $namespace_names
}

function GetKubeResourceTypes
{
    $resource_types = New-Object System.Collections.ArrayList
    $kubectl_get_resources = kubectl api-resources --verbs=list --namespaced -o name
    foreach ($resource in $kubectl_get_resources)
    {
        # Resource name dot separated extended name, store only the first section 
        $resource = $resource.Split(".")
        [void]$resource_types.Add($resource[0])
    }

    $dont_listed_resources = @("roles","rolebinding","clusterrolebinding")
    $dont_listed_resources | ForEach-Object -Process {[void]$resource_types.Add($_)}

    $resource_types = $resource_types | Select-Object -Unique
    return $resource_types
}

function GetKubeResourceName
{
    param
    (
        $ResourceType = "",
        $NamespaceName = ""
    )
     $resource_names = New-Object System.Collections.ArrayList
     $kubectl_get_resource_names = kubectl get $ResourceType --namespace $NamespaceName --no-headers -o custom-columns=":metadata.name"
     foreach($name in $kubectl_get_resource_names)
     {
         [void]$resource_names.Add($name)
     }
     return $resource_names
}

function BackupKubeCluster {
    param (
        $KubeContext,
        $BackupRootPath
    )

    kubectl config use-context $KubeContext
    $KubeNamespaces = New-Object System.Collections.ArrayList
    $KubeResourceTypes = New-Object System.Collections.ArrayList
    $KubeNamespaces = GetKubeNamespaceNames
    $KubeResourceTypes = GetKubeResourceTypes

    $neat_ready = $true
    $neat_check = kubectl neat -h
    if($neat_check -eq $null)
    {
        $neat_ready = $false
    }

    foreach($namespace_name in $KubeNamespaces)
    {
        Write-Host -ForegroundColor Cyan "Namespace processed: " + $namespace_name
        $backup_path = Join-Path -Path $BackupRootPath -ChildPath $namespace_name

        if(!(Test-Path $backup_path -PathType Container))
        {
            New-Item -ItemType Directory -Force -Path $backup_path
        }

        if($neat_ready)
        {
            $namespace_yaml = $namespace_name + ".yaml"
            $namespace_yaml = Join-Path -Path $BackupRootPath $namespace_yaml
            kubectl get namespace $namespace_name -o yaml | kubectl neat > $namespace_yaml
        }
        if(!$neat_ready)
        {
            $namespace_yaml = $namespace_name + ".yaml"
            $namespace_yaml = Join-Path -Path $BackupRootPath $namespace_yaml
            kubectl get namespace $namespace_name -o yaml > $namespace_yaml
        }

        foreach($resource_type in $KubeResourceTypes)
        {
            Write-Host -ForegroundColor Yellow "    - Resource type processed: " + $resource_type
            $kube_resource_names = New-Object System.Collections.ArrayList
            $kube_resource_names = GetKubeResourceName -ResourceType $resource_type -NamespaceName $namespace_name
            Write-Host -ForegroundColor Magenta $kube_resource_names
            foreach($resource_name in $kube_resource_names)
            {
                $yaml_name = $resource_type+"-"+$resource_name+".yaml" -replace(":","_")
                $backup_yaml = Join-Path -Path $backup_path -ChildPath $yaml_name
                Write-Host -ForegroundColor Green "      => " + $backup_yaml
                if($neat_ready)
                {
                    kubectl get $resource_type --namespace $namespace_name $resource_name -o yaml | kubectl neat > $backup_yaml
                }
                if(!$neat_ready)
                {
                    kubectl get $resource_type --namespace $namespace_name $resource_name -o yaml > $backup_yaml
                }
            }
        }
    }
}

Set-PSDebug -Off
BackupKubeCluster -KubeContext $args[0] -BackupRootPath $args[1]


