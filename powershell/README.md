# Backup Kubernetes cluster resources into yaml

Run the command below to backup cluster resources:

```./backup_kube_cluster.ps1 $kubecontext $backuprootpath```

Parameters:
```
$kubecontext = "name_of_kubecontext"
$backuprootpath = "/local/path/to/backup"
```

The script is [neat](https://github.com/itaysk/kubectl-neat) ready.