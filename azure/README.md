# vmss change subnet

The subnet of Azure Kubernetes Service is not changable during on setup process when using webgui. Later if it is necessarry to peer multiple AKS environment the subnets will be overlay.

- Create new subnet for VMSS on Azure webgui
- run ps script to set new subnet for VMSS ```./vmss_change_subnet.ps1 $resource_group $vmss_to_change $new_subnet```

```
$resource_group = "name_of_resource_group"
$vmss_to_change = @('array_of_vmss_names')
$new_subnet = "the Resource ID from properties"
```
