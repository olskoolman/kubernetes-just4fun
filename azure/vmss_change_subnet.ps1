import-module az.compute

function VmssChangeSubnet {
    param (
        $resource_group,
        $vmss_to_change,
        $new_subnet
    )
    foreach ($vmss_name in $vmss_to_change)
    {
        $vmss = Get-AzVmss -ResourceGroupName $resource_group -Name $vmss_name
        #feedback
        Write-Host $vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].Subnet.Id
        $vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].Subnet.Id = $new_subnet
        Update-AzVmss -ResourceGroupName $resource_group -Name $vmss_name -VirtualMachineScaleSet $vmss
        #feedback
        Write-Host $vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].Subnet.Id
    }

}

VmssChangeSubnet -resource_group $args[0] -vmss_to_change $args[1] -new_subnet $args[2]